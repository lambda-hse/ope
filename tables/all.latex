\documentclass{article}

\usepackage{amsmath,amssymb,amsfonts,amsthm}

\begin{document}
    \begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|l|}
\hline & one class & 100 & 1000 & 10000 & 1000000 \\ \hline
 two-class & - & $0.652 \pm 0.031$ & $0.742 \pm 0.011$ & $0.792 \pm 0.004$ & $\mathbf{0.878} \pm 0.000$ \\ \hline
 semi-supervised & $0.378 \pm 0.000$ & $0.715 \pm 0.020$ & $0.766 \pm 0.009$ & $\mathbf{0.847} \pm 0.002$ & $0.876 \pm 0.000$ \\ \hline
 deep-svdd & $0.568 \pm 0.000$ & $0.479 \pm 0.027$ & $0.519 \pm 0.030$ & $0.533 \pm 0.083$ & $0.470 \pm 0.029$ \\ \hline
 brute-force-ope & $0.597 \pm 0.000$ & $0.672 \pm 0.020$ & $0.748 \pm 0.012$ & $0.792 \pm 0.003$ & $0.878 \pm 0.000$ \\ \hline
 hmc-eope & $0.472 \pm 0.000$ & $\mathbf{0.744} \pm 0.021$ & $\mathbf{0.784} \pm 0.008$ & $0.827 \pm 0.003$ & $0.877 \pm 0.000$ \\ \hline
 rmsprop-eope & $0.528 \pm 0.000$ & $0.713 \pm 0.024$ & $0.757 \pm 0.009$ & $0.807 \pm 0.005$ & $0.877 \pm 0.000$ \\ \hline
 deep-eope & $\mathbf{0.652} \pm 0.000$ & $0.688 \pm 0.031$ & $0.778 \pm 0.014$ & $0.817 \pm 0.004$ & $0.877 \pm 0.000$ \\ \hline
\end{tabular}
\caption{SUSY}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.973 \pm 0.005$ & $0.992 \pm 0.008$ & $0.996 \pm 0.004$ \\ \hline
 semi-supervised & $0.978 \pm 0.000$ & $0.871 \pm 0.067$ & $0.953 \pm 0.017$ & $0.973 \pm 0.009$ \\ \hline
 deep-svdd & $0.879 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.211 \pm 0.000$ & $0.978 \pm 0.011$ & $0.995 \pm 0.005$ & $0.997 \pm 0.000$ \\ \hline
 hmc-eope & $0.834 \pm 0.000$ & $0.988 \pm 0.013$ & $0.996 \pm 0.005$ & $0.998 \pm 0.001$ \\ \hline
 rmsprop-eope & $\mathbf{0.993} \pm 0.000$ & $\mathbf{0.999} \pm 0.000$ & $\mathbf{0.998} \pm 0.002$ & $\mathbf{1.000} \pm 0.000$ \\ \hline
 deep-eope & $0.970 \pm 0.000$ & $0.986 \pm 0.016$ & $0.998 \pm 0.003$ & $0.999 \pm 0.001$ \\ \hline
\end{tabular}
\caption{Omniglot-Braille}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.707 \pm 0.081$ & $0.771 \pm 0.036$ & $0.746 \pm 0.058$ \\ \hline
 semi-supervised & $0.433 \pm 0.000$ & $\mathbf{0.723} \pm 0.042$ & $\mathbf{0.772} \pm 0.039$ & $0.734 \pm 0.046$ \\ \hline
 deep-svdd & $0.551 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $\mathbf{0.626} \pm 0.000$ & $0.693 \pm 0.049$ & $0.730 \pm 0.055$ & $\mathbf{0.753} \pm 0.068$ \\ \hline
 hmc-eope & $0.487 \pm 0.000$ & $0.706 \pm 0.033$ & $0.755 \pm 0.035$ & $0.749 \pm 0.060$ \\ \hline
 rmsprop-eope & $0.316 \pm 0.000$ & $0.648 \pm 0.026$ & $0.727 \pm 0.036$ & $0.741 \pm 0.066$ \\ \hline
 deep-eope & $0.602 \pm 0.000$ & $0.699 \pm 0.058$ & $0.751 \pm 0.049$ & $0.743 \pm 0.076$ \\ \hline
\end{tabular}
\caption{Omniglot-Futurama}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.641 \pm 0.072$ & $0.751 \pm 0.042$ & $0.744 \pm 0.022$ \\ \hline
 semi-supervised & $0.779 \pm 0.000$ & $0.600 \pm 0.059$ & $0.734 \pm 0.042$ & $0.715 \pm 0.017$ \\ \hline
 deep-svdd & $0.576 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.706 \pm 0.000$ & $0.703 \pm 0.047$ & $0.775 \pm 0.031$ & $0.770 \pm 0.029$ \\ \hline
 hmc-eope & $0.722 \pm 0.000$ & $0.705 \pm 0.055$ & $0.777 \pm 0.017$ & $0.774 \pm 0.022$ \\ \hline
 rmsprop-eope & $0.756 \pm 0.000$ & $\mathbf{0.828} \pm 0.025$ & $\mathbf{0.825} \pm 0.019$ & $\mathbf{0.840} \pm 0.014$ \\ \hline
 deep-eope & $\mathbf{0.786} \pm 0.000$ & $0.725 \pm 0.079$ & $0.795 \pm 0.019$ & $0.772 \pm 0.016$ \\ \hline
\end{tabular}
\caption{Omniglot-Greek}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 & 8 \\ \hline
 two-class & - & $0.501 \pm 0.213$ & $0.308 \pm 0.235$ & $0.692 \pm 0.355$ & $0.550 \pm 0.247$ \\ \hline
 semi-supervised & $0.973 \pm 0.000$ & $0.294 \pm 0.282$ & $0.222 \pm 0.308$ & $0.825 \pm 0.147$ & $0.732 \pm 0.217$ \\ \hline
 deep-svdd & $0.963 \pm 0.000$ & $0.774 \pm 0.261$ & $0.762 \pm 0.290$ & $0.913 \pm 0.054$ & $0.569 \pm 0.269$ \\ \hline
 brute-force-ope & $0.638 \pm 0.000$ & $0.619 \pm 0.185$ & $0.398 \pm 0.261$ & $0.797 \pm 0.099$ & $0.554 \pm 0.299$ \\ \hline
 hmc-eope & $0.787 \pm 0.000$ & $0.908 \pm 0.144$ & $\mathbf{0.939} \pm 0.047$ & $0.912 \pm 0.056$ & $0.958 \pm 0.025$ \\ \hline
 rmsprop-eope & $\mathbf{0.975} \pm 0.000$ & $\mathbf{0.963} \pm 0.009$ & $0.844 \pm 0.235$ & $\mathbf{0.932} \pm 0.063$ & $\mathbf{0.970} \pm 0.015$ \\ \hline
 deep-eope & $0.961 \pm 0.000$ & $0.812 \pm 0.114$ & $0.498 \pm 0.361$ & $0.850 \pm 0.095$ & $0.766 \pm 0.251$ \\ \hline
\end{tabular}
\caption{KDD}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.700 \pm 0.067$ & $0.779 \pm 0.038$ & $\mathbf{0.829} \pm 0.009$ \\ \hline
 brute-force-ope & $0.454 \pm 0.000$ & $0.706 \pm 0.085$ & $0.746 \pm 0.042$ & $0.806 \pm 0.023$ \\ \hline
 hmc-eope & - & $0.675 \pm 0.094$ & $0.756 \pm 0.028$ & $0.802 \pm 0.018$ \\ \hline
 rmsprop-eope & - & $0.705 \pm 0.050$ & $0.787 \pm 0.053$ & $0.828 \pm 0.005$ \\ \hline
 deep-eope & $\mathbf{0.780} \pm 0.000$ & $\mathbf{0.715} \pm 0.058$ & $\mathbf{0.795} \pm 0.031$ & $0.814 \pm 0.048$ \\ \hline
\end{tabular}
\caption{CIFAR-9}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.749 \pm 0.028$ & $0.726 \pm 0.059$ & $0.772 \pm 0.011$ \\ \hline
 semi-supervised & $0.700 \pm 0.000$ & $0.697 \pm 0.040$ & $0.699 \pm 0.035$ & $0.739 \pm 0.023$ \\ \hline
 deep-svdd & $0.530 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.686 \pm 0.000$ & $0.779 \pm 0.033$ & $0.755 \pm 0.057$ & $0.796 \pm 0.007$ \\ \hline
 hmc-eope & $\mathbf{0.767} \pm 0.000$ & $0.765 \pm 0.037$ & $0.735 \pm 0.028$ & $0.792 \pm 0.038$ \\ \hline
 rmsprop-eope & $0.626 \pm 0.000$ & $0.776 \pm 0.040$ & $\mathbf{0.783} \pm 0.009$ & $\mathbf{0.806} \pm 0.023$ \\ \hline
 deep-eope & $0.687 \pm 0.000$ & $\mathbf{0.779} \pm 0.049$ & $0.768 \pm 0.023$ & $0.799 \pm 0.018$ \\ \hline
\end{tabular}
\caption{CIFAR-0}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.640 \pm 0.061$ & $0.721 \pm 0.028$ & $0.783 \pm 0.022$ \\ \hline
 semi-supervised & $0.477 \pm 0.000$ & $0.576 \pm 0.042$ & $0.653 \pm 0.036$ & $0.701 \pm 0.011$ \\ \hline
 deep-svdd & $0.553 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.472 \pm 0.000$ & $0.668 \pm 0.053$ & $0.735 \pm 0.044$ & $\mathbf{0.792} \pm 0.015$ \\ \hline
 hmc-eope & $0.429 \pm 0.000$ & $0.668 \pm 0.054$ & $0.749 \pm 0.039$ & $0.778 \pm 0.025$ \\ \hline
 rmsprop-eope & $0.500 \pm 0.000$ & $0.654 \pm 0.049$ & $\mathbf{0.760} \pm 0.011$ & $0.789 \pm 0.020$ \\ \hline
 deep-eope & $\mathbf{0.752} \pm 0.000$ & $\mathbf{0.740} \pm 0.039$ & $0.749 \pm 0.038$ & $0.769 \pm 0.022$ \\ \hline
\end{tabular}
\caption{CIFAR-7}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.763 \pm 0.066$ & $0.833 \pm 0.042$ & $0.863 \pm 0.011$ \\ \hline
 semi-supervised & $0.369 \pm 0.000$ & $0.658 \pm 0.031$ & $0.721 \pm 0.028$ & $0.761 \pm 0.007$ \\ \hline
 deep-svdd & $\mathbf{0.547} \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.489 \pm 0.000$ & $0.751 \pm 0.061$ & $0.812 \pm 0.049$ & $0.860 \pm 0.009$ \\ \hline
 hmc-eope & $0.437 \pm 0.000$ & $\mathbf{0.825} \pm 0.013$ & $0.828 \pm 0.027$ & $0.849 \pm 0.007$ \\ \hline
 rmsprop-eope & $0.384 \pm 0.000$ & $0.814 \pm 0.048$ & $\mathbf{0.845} \pm 0.015$ & $\mathbf{0.868} \pm 0.015$ \\ \hline
 deep-eope & $0.503 \pm 0.000$ & $0.816 \pm 0.018$ & $0.768 \pm 0.070$ & $0.836 \pm 0.028$ \\ \hline
\end{tabular}
\caption{CIFAR-1}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.558 \pm 0.069$ & $0.641 \pm 0.048$ & $0.641 \pm 0.042$ \\ \hline
 semi-supervised & $0.569 \pm 0.000$ & $0.565 \pm 0.035$ & $0.616 \pm 0.052$ & $0.625 \pm 0.038$ \\ \hline
 deep-svdd & $0.506 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.514 \pm 0.000$ & $0.609 \pm 0.051$ & $\mathbf{0.650} \pm 0.050$ & $0.670 \pm 0.050$ \\ \hline
 hmc-eope & $0.538 \pm 0.000$ & $\mathbf{0.640} \pm 0.022$ & $0.638 \pm 0.088$ & $\mathbf{0.682} \pm 0.020$ \\ \hline
 rmsprop-eope & $\mathbf{0.621} \pm 0.000$ & $0.596 \pm 0.032$ & $0.615 \pm 0.048$ & $0.671 \pm 0.021$ \\ \hline
 deep-eope & $0.471 \pm 0.000$ & $0.567 \pm 0.019$ & $0.602 \pm 0.033$ & $0.561 \pm 0.024$ \\ \hline
\end{tabular}
\caption{CIFAR-3}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.609 \pm 0.048$ & $0.710 \pm 0.010$ & $0.762 \pm 0.033$ \\ \hline
 semi-supervised & $\mathbf{0.586} \pm 0.000$ & $0.423 \pm 0.019$ & $0.547 \pm 0.014$ & $0.585 \pm 0.017$ \\ \hline
 deep-svdd & $0.540 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.487 \pm 0.000$ & $\mathbf{0.670} \pm 0.075$ & $0.703 \pm 0.019$ & $\mathbf{0.783} \pm 0.037$ \\ \hline
 hmc-eope & $0.531 \pm 0.000$ & $0.642 \pm 0.047$ & $\mathbf{0.733} \pm 0.019$ & $0.774 \pm 0.022$ \\ \hline
 rmsprop-eope & $0.356 \pm 0.000$ & $0.635 \pm 0.020$ & $0.671 \pm 0.049$ & $0.752 \pm 0.005$ \\ \hline
 deep-eope & $0.537 \pm 0.000$ & $0.544 \pm 0.088$ & $0.508 \pm 0.030$ & $0.632 \pm 0.059$ \\ \hline
\end{tabular}
\caption{CIFAR-6}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.585 \pm 0.051$ & $0.550 \pm 0.019$ & $0.611 \pm 0.013$ \\ \hline
 semi-supervised & $\mathbf{0.679} \pm 0.000$ & $0.483 \pm 0.026$ & $0.470 \pm 0.005$ & $0.547 \pm 0.028$ \\ \hline
 deep-svdd & $0.506 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.498 \pm 0.000$ & $0.596 \pm 0.036$ & $0.575 \pm 0.004$ & $0.634 \pm 0.021$ \\ \hline
 hmc-eope & $0.576 \pm 0.000$ & $0.591 \pm 0.076$ & $0.595 \pm 0.046$ & $0.641 \pm 0.016$ \\ \hline
 rmsprop-eope & $0.582 \pm 0.000$ & $\mathbf{0.635} \pm 0.022$ & $\mathbf{0.658} \pm 0.031$ & $\mathbf{0.667} \pm 0.022$ \\ \hline
 deep-eope & $0.476 \pm 0.000$ & $0.582 \pm 0.031$ & $0.616 \pm 0.055$ & $0.639 \pm 0.063$ \\ \hline
\end{tabular}
\caption{CIFAR-2}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.711 \pm 0.055$ & $0.785 \pm 0.009$ & $0.824 \pm 0.026$ \\ \hline
 semi-supervised & - & $0.682 \pm 0.087$ & $0.741 \pm 0.033$ & $0.784 \pm 0.022$ \\ \hline
 deep-svdd & $0.590 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.672 \pm 0.000$ & $0.760 \pm 0.037$ & $0.801 \pm 0.014$ & $0.831 \pm 0.020$ \\ \hline
 hmc-eope & $0.713 \pm 0.000$ & $0.751 \pm 0.054$ & $0.805 \pm 0.004$ & $0.839 \pm 0.031$ \\ \hline
 rmsprop-eope & - & $\mathbf{0.790} \pm 0.045$ & $\mathbf{0.851} \pm 0.011$ & $\mathbf{0.857} \pm 0.019$ \\ \hline
 deep-eope & $\mathbf{0.725} \pm 0.000$ & $0.682 \pm 0.045$ & $0.752 \pm 0.040$ & $0.703 \pm 0.041$ \\ \hline
\end{tabular}
\caption{CIFAR-8}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.585 \pm 0.068$ & $0.620 \pm 0.019$ & $0.650 \pm 0.027$ \\ \hline
 semi-supervised & $\mathbf{0.709} \pm 0.000$ & $0.447 \pm 0.069$ & $0.463 \pm 0.034$ & $0.531 \pm 0.026$ \\ \hline
 deep-svdd & $0.501 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.454 \pm 0.000$ & $0.603 \pm 0.062$ & $0.616 \pm 0.048$ & $0.667 \pm 0.032$ \\ \hline
 hmc-eope & $0.493 \pm 0.000$ & $0.616 \pm 0.043$ & $\mathbf{0.646} \pm 0.032$ & $\mathbf{0.694} \pm 0.032$ \\ \hline
 rmsprop-eope & $0.644 \pm 0.000$ & $\mathbf{0.666} \pm 0.053$ & $0.636 \pm 0.003$ & $0.673 \pm 0.042$ \\ \hline
 deep-eope & $0.419 \pm 0.000$ & $0.505 \pm 0.065$ & $0.552 \pm 0.038$ & $0.496 \pm 0.016$ \\ \hline
\end{tabular}
\caption{CIFAR-4}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline & one class & 1 & 2 & 4 \\ \hline
 two-class & - & $0.697 \pm 0.023$ & $0.695 \pm 0.036$ & $0.736 \pm 0.017$ \\ \hline
 semi-supervised & $0.563 \pm 0.000$ & $0.644 \pm 0.033$ & $0.676 \pm 0.020$ & $0.684 \pm 0.027$ \\ \hline
 deep-svdd & $0.556 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ & $0.500 \pm 0.000$ \\ \hline
 brute-force-ope & $0.628 \pm 0.000$ & $0.722 \pm 0.027$ & $0.730 \pm 0.021$ & $\mathbf{0.742} \pm 0.006$ \\ \hline
 hmc-eope & $0.609 \pm 0.000$ & $\mathbf{0.733} \pm 0.022$ & $\mathbf{0.740} \pm 0.027$ & $0.740 \pm 0.027$ \\ \hline
 rmsprop-eope & $\mathbf{0.680} \pm 0.000$ & $0.701 \pm 0.016$ & $0.686 \pm 0.063$ & $0.737 \pm 0.015$ \\ \hline
 deep-eope & $0.641 \pm 0.000$ & $0.577 \pm 0.034$ & $0.629 \pm 0.040$ & $0.601 \pm 0.031$ \\ \hline
\end{tabular}
\caption{CIFAR-5}
\end{figure}


\begin{figure}
\centering
\begin{tabular}{|l|l|l|l|l|l|}
\hline & one class & 100 & 1000 & 10000 & 1000000 \\ \hline
 two-class & - & $0.496 \pm 0.017$ & $0.529 \pm 0.007$ & $0.566 \pm 0.006$ & $0.858 \pm 0.002$ \\ \hline
 semi-supervised & $\mathbf{0.535} \pm 0.000$ & $0.497 \pm 0.011$ & $0.521 \pm 0.004$ & $0.604 \pm 0.002$ & $0.742 \pm 0.006$ \\ \hline
 deep-svdd & $0.487 \pm 0.000$ & $0.500 \pm 0.007$ & $0.497 \pm 0.009$ & $0.498 \pm 0.006$ & $0.499 \pm 0.007$ \\ \hline
 brute-force-ope & $0.508 \pm 0.000$ & $0.500 \pm 0.009$ & $0.520 \pm 0.003$ & $0.572 \pm 0.005$ & $0.859 \pm 0.001$ \\ \hline
 hmc-eope & $0.491 \pm 0.000$ & $0.523 \pm 0.005$ & $\mathbf{0.567} \pm 0.008$ & $\mathbf{0.648} \pm 0.005$ & $0.848 \pm 0.001$ \\ \hline
 rmsprop-eope & $0.497 \pm 0.000$ & $0.497 \pm 0.004$ & $0.532 \pm 0.007$ & $0.603 \pm 0.007$ & $\mathbf{0.860} \pm 0.001$ \\ \hline
 deep-eope & $0.531 \pm 0.000$ & $\mathbf{0.530} \pm 0.008$ & $0.565 \pm 0.008$ & $0.631 \pm 0.003$ & $0.860 \pm 0.001$ \\ \hline
\end{tabular}
\caption{HIGGS}
\end{figure}

\end{document}