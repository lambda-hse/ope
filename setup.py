"""
(1 + eps)-class classification.
"""

from setuptools import setup, find_packages
from codecs import open
import os.path as osp
import numpy as np

here = osp.abspath(osp.dirname(__file__))

with open(osp.join(here, 'README.md'), encoding='utf-8') as f:
  long_description=f.read()

setup(
  name='ope',

  version='0.1.0',

  description="""(1 + eps)-class classification.""",

  long_description = long_description,

  url='https://gitlab.com/mborisyak/ope',

  author='Maxim Borisyak and contributors.',
  author_email='mborisyak at hse . ru',

  maintainer='Maxim Borisyak',
  maintainer_email='mborisyak at hse . ru',

  license='MIT',

  classifiers=[
    'Development Status :: 4 - Beta',

    'Intended Audience :: Science/Research',

    'Topic :: Scientific/Engineering :: Image Recognition',

    'License :: OSI Approved :: MIT License',

    'Programming Language :: Python :: 3',
  ],

  keywords='neural networks',

  packages=find_packages(exclude=['contrib', 'examples', 'docs', 'tests']),

  extras_require={
    'dev': ['check-manifest'],
    'test': ['nose>=1.3.0'],
  },

  install_requires=[
    'numpy',
    'scikit-learn'
  ],

  include_package_data=True,
  ext_modules=[],
  include_dirs=[np.get_include()]
)