import numpy as np

def merge_methods(results):
  from ope.experiment import ope_to_oc_mapping, oc_to_ope_mapping

  merged = dict()

  for model in results:
    if model in oc_to_ope_mapping:
      continue

    merged[model] = results[model].copy()

    if model in ope_to_oc_mapping:

      companion_model = ope_to_oc_mapping[model]

      if companion_model not in results:
        companion_model = companion_model.replace('eoc', 'oc')

      if companion_model not in results:
        import warnings
        warnings.warn('Missing one class model for %s' % (model, ))
        continue

      assert len(results[companion_model].keys()) == 1, (
        companion_model,
        list(results[companion_model].keys())
      )

      oc_partition, = results[companion_model].keys()

      merged[model][oc_partition] = results[companion_model][oc_partition]

  return merged


def split_results(results):
  from ope.experiment import ope_models, one_class_models
  ope_results = dict()
  oc_results = dict()

  for model in results:
    if model in ope_models:
      ope_results[model] = results[model]
    elif model in one_class_models:
      oc_results[model] = results[model]
    else:
      raise ValueError('Unknown model %s' % (model, ))

  return ope_results, oc_results


def argmax(keys, values):
  max = 0
  arg = None

  for k, v in zip(keys, values):
    if v is None:
      continue

    if np.mean(v) > max:
      max = np.mean(v)
      arg = k

  return arg

def format_latext(rows, headers):
  header =  r'\hline & {header} \\ \hline'.format(
    header=' & '.join(headers)
  )

  body = '\n'.join([
    ' {row} \\\\ \\hline'.format(
      row=' & '.join(row)
    )
    for row in rows
  ])

  return '\\begin{tabular}{%s}\n%s\n%s\n\\end{tabular}' % (
    '|l' * (len(headers) + 1) + '|',
    header,
    body
  )



def format_value(m, s, best=False, format='txt'):
  if format == 'txt':
    if best:
      return '*%.3lf* +- %.3lf' % (m, s)
    else:
      return ' %.3lf  +- %.3lf' % (m, s)
  else:
    if best:
      return r'$\mathbf{%.3lf} \pm %.3lf$' % (m, s)
    else:
      return r'$%.3lf \pm %.3lf$' % (m, s)

def table(results, format='txt'):
  rows = []

  all_models = [
    model
    for model in one_class_models
    if model in results
  ] + [
    model
    for model in ope_models
    if model in results
  ]

  all_partitions = list(set([
    partition
    for model in results
    for partition in results[model]
  ]))

  all_partitions = sorted(all_partitions, key=lambda a: a[0] if a[0] is not None else -1)

  header = [
    str(partition[0]) if partition[0] is not None else 'one class'
    for partition  in all_partitions
  ]

  best = {
    partition : argmax(all_models, [
      results[m].get(partition, None) for m in all_models
    ])
    for partition in all_partitions
  }

  for model in all_models:
    row = [str(model)]

    for partition in all_partitions:

      if partition not in results[model]:
        row.append('-')
        continue

      scores = results[model][partition]
      row.append(
        format_value(np.mean(scores), np.std(scores), best=(model == best[partition]), format=format)
      )

    rows.append(row)
  if format == 'txt':
    from tabulate import tabulate
    return tabulate(rows, headers=header, disable_numparse=True)
  else:
    return format_latext(rows, headers=header)


if __name__ == '__main__':
  import os
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument('--root', type=str, default='results/')
  parser.add_argument('--summary', default=None)
  parser.add_argument('--outdir', type=str, default='tables/')
  parser.add_argument('--format', choices=['txt', 'latex'], type=str, default='txt')

  args = parser.parse_args()

  root = args.root
  outdir = args.outdir
  format = args.format

  from ope.experiment import *

  if args.summary is not None:
    root = args.summary

    if os.path.isdir(root):
      results = dict()
      for item in os.listdir(root):
        path = os.path.join(root, item)

        import pickle
        with open(path, 'rb') as f:
          results.update(
            pickle.load(f)
          )
    else:
      import pickle

      with open(root, 'rb') as f:
        results =  pickle.load(f)
  else:
    results = load_results(root)

  sresults = {
    dataset: select_best_hyperparams(results[dataset])
    for dataset in results
  }

  print(sresults)
  merged = dict()

  for dataset in sresults:
    print(dataset)
    merged[dataset] = merge_methods(sresults[dataset])

  os.makedirs(outdir, exist_ok=True)

  all_tables = []

  for dataset in merged:
    t = table(merged[dataset], args.format)
    all_tables.append((dataset, t))

    path = os.path.join(outdir, '%s.%s' % (dataset, format))

    with open(path, 'w') as f:
      f.write(t)

  if format == 'txt':
    with open(os.path.join(outdir, 'all.txt'), 'w') as f:
      f.write('\n\n'.join(
        '== %s == \n%s\n' % (dataset, t)
        for dataset, t in all_tables
      ))
  else:
    here = os.path.dirname(__file__)

    from string import Template

    with open(os.path.join(here, 'template.latex')) as f:
      document_template = Template(f.read())

    source_path = os.path.join(outdir, 'all.latex')

    with open(source_path, 'w') as f:
      table_template = '\\begin{figure}\n\\centering\n%s\n\\caption{%s}\n\\end{figure}\n'

      f.write(
        document_template.substitute(
          body='\n\n'.join(
            table_template % (t, dataset)
            for dataset, t in all_tables
          )
        )
      )

    try:
      import subprocess as sp
      import tempfile

      with tempfile.TemporaryDirectory() as tmp:
        sp.run([
          'cp', os.path.join(outdir, 'all.latex'), os.path.join(tmp, 'all.latex')
        ])

        sp.run([
          'xelatex', '-synctex=1', '-shell-escape', '-interaction=nonstopmode',
          os.path.join(tmp, 'all.latex')
        ], cwd=tmp)

        sp.run([
          'mv', os.path.join(tmp, 'all.pdf'), os.path.join(outdir, 'all.pdf')
        ])

    except:
      import traceback
      traceback.print_exc()
