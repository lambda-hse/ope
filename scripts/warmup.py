def warmup(task_name):
  from ope.experiment import one_class_datasets, two_class_datasets

  if task_name in two_class_datasets:
    try:
      task, _, _ = two_class_datasets[task_name]
      _ = task()
    except AttributeError:
      print('%s has been preloaded (probably).' % task_name)
      import traceback
      traceback.print_exc()
  else:
    try:
      task, _, _ = one_class_datasets[task_name]
      _ = task()
    except AttributeError:
      print('%s has been preloaded (probably).' % task_name)
      import traceback
      traceback.print_exc()

if __name__ == '__main__':
  import argparse
  from ope.experiment import available_datasets

  parser = argparse.ArgumentParser()
  parser.add_argument(
    'task', choices=['all'] + available_datasets, nargs='+'
  )

  arguments = parser.parse_args()

  if 'all' in arguments.task:
    assert len(arguments.task) == 1
    tasks = available_datasets
  else:
    tasks = arguments.task

  for task in tasks:
    warmup(task)
