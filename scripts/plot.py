import matplotlib.pyplot as plt
import numpy as np


def plot(results, figsize=(9, 6)):
  plt.figure(figsize=figsize)
  plt.yscale('log')

  min_xs = 999999999
  max_xs = 0

  for model in results:
    print(model)
    xs = []
    means = []
    stds = []

    if len(results[model]) > 1:
      for partiton in results[model]:
        xs.append(partiton[0])
        means.append(np.mean(results[model][partiton]))
        if len(results[model][partiton]) > 1:
          stds.append(np.std(results[model][partiton]))
        else:
          stds.append(0)

      indx = np.argsort(xs)
      xs = np.array(xs)[indx]
      means = np.array(means)[indx]
      stds = np.array(stds)[indx]

      plt.errorbar(xs, means, yerr=stds, label=model)

      min_xs = min_xs if np.min(xs) > min_xs else np.min(xs)
      max_xs = np.max(xs) if np.max(xs) > max_xs else max_xs

  for model in results:
    if len(results[model]) == 1:
      partition, = results[model].keys()
      means = np.mean(results[model][partition])
      plt.plot([min_xs, max_xs], [means, means], label=model)

  plt.legend()

if __name__ == '__main__':
  import os
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument('--root', type=str, default='results/')
  parser.add_argument('--summary', default=None)
  parser.add_argument('--outdir', type=str, default='figures/')
  parser.add_argument('--format', type=str, default='png')
  parser.add_argument('--width', type=float, default=9)
  parser.add_argument('--height', type=float, default=6)

  args = parser.parse_args()

  root = args.root
  outdir = args.outdir
  format = args.format
  w, h = args.width, args.height

  from ope.experiment import *

  if args.summary is not None:
    root = args.summary

    if os.path.isdir(root):
      results = dict()
      for item in os.listdir(root):
        path = os.path.join(root, item)

        import pickle
        with open(path, 'rb') as f:
          results.update(
            pickle.load(f)
          )
    else:
      import pickle

      with open(root, 'rb') as f:
        results =  pickle.load(f)
  else:
    results = load_results(root)

  sresults = {
    dataset: select_best_hyperparams(results[dataset])
    for dataset in results
  }

  print(sresults)

  os.makedirs(outdir, exist_ok=True)

  for dataset in sresults:
    plot(sresults[dataset], figsize=(w, h))
    plt.title(dataset)

    path = os.path.join(outdir, '%s.%s' % (dataset, format))
    plt.savefig(path, format=format)
    plt.close()


