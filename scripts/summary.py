def main(root, target):
  from ope.experiment.utils import load_results

  import pickle

  summary = load_results(root, min_iterations=0)

  with open(target, 'wb') as f:
    pickle.dump(summary, f)

if __name__ == '__main__':
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument(
    'root', type=str
  )

  parser.add_argument(
    'target', type=str
  )

  arguments = parser.parse_args()

  main(arguments.root, arguments.target)