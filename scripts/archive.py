def main(root, target_archive):
  import os
  from ope.experiment.utils import walk
  from ope.experiment.task import find_latest
  from tqdm import tqdm

  items = set([
    item[:-1]
    for item in walk(root)
  ])

  import tarfile

  with tarfile.open(target_archive, mode='w') as f:
    pbar = tqdm(items)
    max_len = max(len(item[0]) for item in items)

    for item in pbar:
      model_root = os.path.join(root, '/'.join(item))
      latest = find_latest(model_root)

      if latest is not None:
        pbar.set_description(item[0].ljust(max_len))
        f.add(latest)

if __name__ == '__main__':
  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument(
    'root', type=str
  )

  parser.add_argument(
    'target', type=str
  )

  arguments = parser.parse_args()

  main(arguments.root, arguments.target)