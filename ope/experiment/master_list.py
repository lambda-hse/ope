from ope.models import *
from ope.datasets import *

from ope.models.utils import *

EPSILON=0.95

__all__ = [
  'ope_models', 'one_class_models',
  'two_class_datasets', 'one_class_datasets',
  'available_datasets', 'available_models',
  'ope_to_oc_mapping', 'oc_to_ope_mapping',

  'available_models_tokenized',

  'get_dataset_model'
]

ope_models = {
  'two-class' : {
    ('two-class', ) : cross_entropy()
  },

  'semi-supervised': {
    ('semi-supervised',): semi_supervised()
  },

  'deep-svdd' : {
    ('deep-svdd', ) : deep_svdd()
  },

  'brute-force-ope' : {
    ('brute-force-ope', EPSILON) : brute_force_ope(EPSILON)
  },

  'hmc-eope' : {
    ('hmc-eope', EPSILON) : energy_ope(
      EPSILON, persistent=True,
      chain=hmc_chain(num_steps=4)
    )
  },

  'rmsprop-eope' : {
    ('rmsprop-eope', EPSILON) : energy_ope(
      EPSILON, persistent=True,
      chain=rmsprop_chain(num_steps=4, step_size=1e-2, noise=1)
    )
  },

  'deep-eope' : {
    ('deep-eope', EPSILON) : bideep_energy_ope(EPSILON)
  },
}

one_class_models = {
  'ae-oc' : {
    ('ae-oc', ) : ae_oc()
  },

  'deep-svdd-oc' : {
    ('deep-svdd-oc', ) : deep_svdd_oc()
  },

  'brute-force-oc' : {
    ('brute-force-oc', EPSILON) : brute_force_oc(EPSILON)
  },

  'hmc-eoc' : {
    ('hmc-eoc', EPSILON) : energy_oc(
      EPSILON, persistent=True,
      chain=hmc_chain(num_steps=4, step_size=1e-2)
    )
  },

  'rmsprop-eoc' : {
    ('rmsprop-eoc', EPSILON) : energy_oc(
      EPSILON, persistent=True,
      chain=rmsprop_chain(num_steps=4, step_size=1e-2, noise=1)
    )
  },

  'deep-eoc' : {
    ('deep-eoc', EPSILON) : bideep_energy_oc(EPSILON)
  },
}

ope_to_oc_mapping = {
  'semi-supervised' : 'ae-oc',
  'brute-force-ope' : 'brute-force-oc',
  'hmc-eope' : 'hmc-eoc',
  'rmsprop-eope' : 'rmsprop-eoc',
  'deep-eope' : 'deep-eoc',
  'deep-svdd' : 'deep-svdd-oc',
}

oc_to_ope_mapping = dict(
  (v, k) for k, v in ope_to_oc_mapping.items()
)

def concat(*dicts):
  result = dict()
  for d in dicts:
    result.update(d)
  return result

two_class_datasets = concat(
  {
    'moons' : (moons(seed=1111), [(4, ), (8, ), (16, )], [3, 3, 3]),
    'higgs' : (higgs(seed=1112), [(100, ), (1000, ), (10000, ), (1000000, )], [8, 6, 4, 4]),
    'susy'  : (susy(seed=1113),  [(100, ), (1000, ), (10000, ), (1000000, )], [8, 6, 4, 4]),
    'kdd'  :  (kdd(seed=1114),   [(1, 1000), (2, 1000), (4, 1000), (8, 1000)], [8, 6, 4, 4]),
  }, {
    ('mnist-%d' % (digit, )) : (mnist(digit, seed=1114 + digit), [(1, 10), (2, 10), (4, 10)], [5, 3, 3])
    for digit in range(10)
  }, {
    'cifar-%d' % (normal, ) : (cifar10(normal, seed=1124 + normal), [(1, 10), (2, 10), (4, 10)], [5, 3, 3])
    for normal in range(10)
  }, {
    'omniglot-%s' % (normal, ) : (omniglot(normal, seed=1134 + i), [(1, 10), (2, 10), (4, 10)], [5, 5, 5])
    for i, normal in enumerate(['Futurama', 'Greek', 'Braille'])
  }

)

def get_one_class_dataset(dataset, partitions, repeats):
  one_class_partitions = [ (None, ) * len(partitions[0]) ]
  one_class_repeats = [1, ]
  return dataset, one_class_partitions, one_class_repeats

one_class_datasets = {
  name : get_one_class_dataset(*two_class_datasets[name])
  for name in two_class_datasets
}

available_datasets = list(set(
  list(two_class_datasets.keys()) + list(one_class_datasets.keys())
))

available_models = list(set(
  list(ope_models.keys()) + list(one_class_models.keys())
))

available_models_tokenized = list(set([
  token for model in available_models for token in model.split('-')
]))

def get_dataset_model(task_name, model_name):
  if model_name in ope_models:
    return two_class_datasets[task_name], ope_models[model_name]
  else:
    return one_class_datasets[task_name], one_class_models[model_name]