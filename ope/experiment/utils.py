import os
import pickle

import numpy as np

from .task import TaskState

__all__ = [
  'load_results',
  'bind',
  'select_best_hyperparams'
]

def bind(f, values):
  import inspect

  signature = inspect.signature(f)
  args = dict()

  for name in signature.parameters:
    args[name] = values[name]()

  return args

def walk(root):
  for item in os.listdir(root):
    path = os.path.join(root, item)

    if os.path.isdir(path):
      for subitem in walk(path):
        yield (item,) + subitem
    else:
      yield (item,)

import re
version_regexp = re.compile('^(\d+).pickled$')

def get_version(item):
  match = version_regexp.fullmatch(item)
  return int(match.groups()[0])

def load_results(root, min_iterations=32):
  index = dict()

  for item in walk(root):
    try:
      dataset, partition, model, repeat, f = item
    except:
      print('Failed to process %s' % (item, ))
      continue

    key = (dataset, partition, model, repeat)

    version = get_version(f)
    path = os.path.join(root, *key, f)

    if version < min_iterations:
      continue

    if key not in index:
      index[key] = (version, path)
    else:
      previous_version, _ = index[key]
      if previous_version < version:
        index[key] = (version, path)

  results = dict()
  for key in index:
    version, path = index[key]

    with open(path, 'rb') as f:
      state = pickle.load(f)
      assert isinstance(state, TaskState)

    dataset = state.dataset
    partition = state.partition
    model = state.model
    hyperparameters = tuple(state.hyperparameters.items())

    if dataset not in results:
      results[dataset] = dict()

    if model not in results[dataset]:
      results[dataset][model] = dict()

    if partition not in results[dataset][model]:
      results[dataset][model][partition] = dict()

    if hyperparameters not in results[dataset][model][partition]:
      results[dataset][model][partition][hyperparameters] = list()

    results[dataset][model][partition][hyperparameters].append(
      (state.val_scores, state.test_scores)
    )

  return results

def select_best_hyperparams(results):
  selected = dict()

  for model in results:
    selected[model] = dict()

    for partition in results[model]:
      d = results[model][partition]
      params = [ p for p in d ]

      if any([ val_score is None for p in params for val_score, test_score in d[p] ]):
        selected[model][partition] = [
          test_score
          for param in params
          for val_score, test_score in d[param]
        ]
      else:
        val_scores = [
          np.mean([val_score for val_score, test_score in d[p]])
          for p in params
        ]

        best = int(np.argmax(val_scores))

        selected[model][partition] = [
          test_score
          for val_score, test_score in d[params[best]]
        ]

  return selected
