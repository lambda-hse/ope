import os
from .task import Task

__all__ = [
  'Experiment',
  'ExperimentInitializer'
]

class ExperimentInitializer(object):
  def __init__(self, session, dataset, models, partitions, repeats, root, override=False):
    from craynn import variable_dataset

    self.session = session
    self.dataset = dataset()
    self.models = models
    self.partitions = partitions
    self.repeats = repeats
    self.root = root

    self.override = override

    if not os.path.exists(root):
      os.makedirs(root, exist_ok=True)

    self.tasks = dict()

    self.data_pos = variable_dataset(session, self.dataset.data_pos)
    self.data_neg = variable_dataset(session, self.dataset.data_neg)
    self.data_test = variable_dataset(session, self.dataset.data_test)

  def __len__(self):
    return sum([
      len(self.models) * repeat
      for repeat in self.repeats
    ])

  def initialize(self):
    i = 0
    for partition, repeat in zip(self.partitions, self.repeats):
      for hyperparameters, model in self.models.items():
        for model_id in range(repeat):
          model_name = hyperparameters[0]

          partition_key = '-'.join([str(x) for x in partition])
          model_key = '-'.join([str(x) for x in hyperparameters])

          task_root = os.path.join(self.root, self.dataset.name(), partition_key, model_key, '%03d' % model_id)
          os.makedirs(task_root, exist_ok=True)

          assert (partition, hyperparameters, model_id) not in self.tasks

          self.tasks[partition, hyperparameters, model_id] = Task(
            self.session,
            model_name, model,
            self.dataset,
            self.data_pos, self.data_neg, self.data_test,
            partition, task_root,
            override=self.override
          )
          i += 1
          yield i

  def __call__(self):
    return Experiment(
      self.session, self.dataset,
      self.data_pos, self.data_neg, self.data_test,
      self.tasks, self.root
    )

class Experiment(object):
  def __init__(self, session, dataset, data_pos, data_neg, data_test, tasks, root):
    self.root = root

    self.session = session
    self.dataset = dataset

    self.data_pos = data_pos
    self.data_neg = data_neg
    self.data_test = data_test

    self.tasks = tasks

    if not os.path.exists(root):
      os.makedirs(root, exist_ok=True)

  def train(self, iterations, n_steps_per_epoch=None):
    for i in range(iterations):
      self.epoch(i, n_steps_per_epoch)
      yield i

  def epoch(self, iteration, n_steps=None):
    if n_steps is None:
      n_steps = self.dataset.data_pos.shape[0] // self.dataset.batch_size

    active = [
      key for key in self.tasks
      if self.tasks[key].iteration <= iteration
    ]

    if len(active) == 0:
      return

    updates = [
      self.tasks[key].updates()
      for key in active
    ]

    for _ in range(n_steps):
      self.session.run(updates)

    for key in active:
      self.tasks[key].iteration += 1

    scores_test = self.score(
      predictions=[self.tasks[key].predictions_test for key in active],
      dataset=self.data_test,
      labels=self.dataset.labels_test
    )

    for key, score_test in zip(active, scores_test):
      self.tasks[key].save(score_test=score_test)

  def score(self, predictions, dataset, labels):
    from sklearn.metrics import roc_auc_score

    probas = dataset.eval(self.session, *predictions, batch_size=self.dataset.batch_size)

    try:
      return [
        roc_auc_score(labels, proba)
        for proba in probas
      ]
    except Exception as e:
      raise ValueError(str(
        (labels.shape, [proba.shape for proba in probas])
      )) from e