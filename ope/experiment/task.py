import os
from collections import namedtuple

import numpy as np

__all__ = [
  'Task'
]

def bind_networks(model, dataset):
  network_pool = dataset.network_pool()
  import inspect

  signature = inspect.signature(model)
  network_models = dict()

  for name in signature.parameters:
    if name not in network_pool:
      raise ValueError('Can not find network of type %s in %s' % (name, dataset.name(),))

    network_models[name] = network_pool[name]

  return dict([
    (name, network_models[name]())
    for name in network_models
  ])

def find_latest(root):
  import re

  if not os.path.exists(root) or not os.path.isdir(root):
    return None

  items = os.listdir(root)

  regexp = re.compile('^(\d+).pickled$')

  versions = []
  nums = []

  for item in items:
    match = regexp.fullmatch(item)
    if match is None:
      continue

    num, = match.groups()
    nums.append(int(num))
    versions.append(item)

  if len(versions) > 0:
    indx = int(np.argmax(nums))
    path = os.path.join(root, versions[indx])
    return path
  else:
    return None

def load_latest(root):
  path = find_latest(root)

  if path is not None:
    import pickle

    with open(path, 'rb') as f:
      state = pickle.load(f)

    assert isinstance(state, TaskState)

    return state
  else:
    return None

def get_subset(session, dataset, indx):
  from craynn import indexed_subset

  if isinstance(indx, np.ndarray):
    return indexed_subset(session, dataset, indx)

  elif indx is None:
    return None

  elif indx == slice(None, None, None):
    return dataset

  else:
    raise ValueError(str(indx))

def get_size(data, indx):
  if isinstance(indx, np.ndarray):
    return indx.shape[0]

  elif indx == slice(None, None, None):
    return data.shape[0]

  elif indx is None:
    return 0

  else:
    raise ValueError(str(indx))

TaskState = namedtuple('TaskState', [
  'dataset', 'partition',
  'model', 'hyperparameters',
  'iteration',

  'networks_state',
  'optimizer_state',
  'dataset_indices',

  'val_scores', 'test_scores'
])

class Task(object):
  def __init__(
    self, session, model_name, model,
    dataset, data_pos, data_neg, data_test,
    partition, root, optimizer=None,
    override=False
  ):
    import tensorflow as tf
    from craynn import tf_updates

    if optimizer is None:
      optimizer=tf_updates.adam(2e-4)

    self.model_name = model_name

    self.root = root

    self.session = session

    self.dataset = dataset

    self.data_pos = data_pos
    self.data_neg = data_neg
    self.data_test = data_test

    self.partition = partition

    state = load_latest(root) if not override else None

    self.iteration = state.iteration if state is not None else 0
    self.networks = bind_networks(model, self.dataset)

    if state is None:
      self.session.run([
        nn.reset() for nn in self.networks.values()
      ])
    else:
      for k in self.networks:
        self.session.run(**self.networks[k].assign(state.networks_state[k]))

    if state is not None:
      indx_pos, indx_neg = state.dataset_indices
      self.dataset_indices = state.dataset_indices
    else:
      indx_pos, indx_neg = self.dataset.subset(*partition)
      self.dataset_indices = (indx_pos, indx_neg)

    dataset_pos = get_subset(self.session, self.data_pos, indx_pos)
    dataset_neg = get_subset(self.session, self.data_neg, indx_neg)
    batch_size = self.dataset.batch_size

    X_pos, = dataset_pos.batch(batch_size) if dataset_pos is not None else (None, )
    X_neg, = dataset_neg.batch(batch_size) if dataset_neg is not None else (None, )

    if X_pos is not None:
      X_pos = tf.reshape(X_pos, (batch_size, ) + self.dataset.data_pos.shape[1:])

    if X_neg is not None:
      X_neg = tf.reshape(X_neg, (batch_size, ) + self.dataset.data_pos.shape[1:])

    self.X_pos, self.X_neg = X_pos, X_neg
    bounding_box = self.dataset.bounding_box()

    alpha = get_size(self.dataset.data_neg, indx_neg) / get_size(self.dataset.data_pos, indx_pos)

    self.model = model(**self.networks)(X_pos, X_neg, alpha, bounding_box)

    self.session.run(self.model.reset())

    self.loss_value, self.grads = self.model.loss_and_grads()

    with tf.control_dependencies([self.model.updates()]):
      self.optimizer = optimizer(self.loss_value, None, self.grads)

    if state is None:
      self.session.run(self.optimizer.reset())
    else:
      self.session.run(**self.optimizer.assign(state.optimizer_state))

    X_test, = self.data_test.seq_batch()
    self.predictions_test = self.model(X_test)

    self._upds = tf.group([
      self.model.updates(),
      self.optimizer.updates()
    ])

  def updates(self):
    return self._upds

  def score(self, pred, dataset, labels):
    from sklearn.metrics import roc_auc_score

    proba = dataset.eval(self.session, pred, batch_size=self.dataset.batch_size)

    try:
      return roc_auc_score(labels, proba)
    except Exception as e:
      raise ValueError(str((labels.shape, proba.shape))) from e

  def save(self, score_val=None, score_test=None):
    import pickle

    if not os.path.exists(self.root):
      os.makedirs(self.root, exist_ok=True)

    if score_test is None:
      score_test = self.score(self.predictions_test, self.data_test, self.dataset.labels_test)

    networks_state = self.session.run({
      k : self.networks[k].variables()
      for k in self.networks
    })

    optimizer_state = self.session.run(self.optimizer.state())

    state = TaskState(
      dataset=self.dataset.name(),
      partition=self.partition,

      model=self.model_name,
      hyperparameters=self.model.hyperparameters(),

      iteration=self.iteration,
      networks_state=networks_state,
      optimizer_state=optimizer_state,
      dataset_indices=self.dataset_indices,

      val_scores=score_val,
      test_scores=score_test
    )

    path = os.path.join(self.root, '%06d.pickled' % (self.iteration, ))

    with open(path, 'wb') as f:
      pickle.dump(state, f)