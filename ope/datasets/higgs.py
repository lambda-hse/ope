from .meta import *

__all__ = [
  'higgs'
]


class HIGGS(UngroupedDataset):
  def __init__(self, seed, root=None):
    from craynn import datasets, utils

    data, labels = datasets.dataflow(
      datasets.download_higgs('HIGGS/'),
      datasets.read_higgs @ datasets.pickled('HIGGS/higgs.pickled')
    )(root)

    data_train, labels_train, data_test, labels_test = utils.split(
      data, labels,
      split_ratios=(10, 1),
      seed=seed
    )

    data_pos = data_train[labels_train == 1]
    data_neg = data_train[labels_train == 0]

    super(HIGGS, self).__init__(
      data_pos, data_neg,
      data_test, labels_test,
      batch_size=32,
      seed=seed
    )

  def network_pool(self):
    from .common import get_dense_network_pool

    return get_dense_network_pool(
      num_features=28,
      n=96,
      code_size=14,
      latent_size=64
    )

higgs = lambda seed, root=None: lambda: HIGGS(seed, root)