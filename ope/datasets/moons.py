import numpy as np

from .meta import *

__all__ = [
  'moons'
]


class Moons(UngroupedDataset):
  def __init__(self, seed):
    from sklearn.datasets import make_moons

    get_pos = lambda size, seed: make_moons(n_samples=size, noise=0.05, random_state=seed)[0].astype('float32')

    data_pos = get_pos(64, seed)
    data_pos_test = get_pos(64, seed + 2)

    center = np.array([0.5, 0.25], dtype='float32')
    X_range = np.array([
      [-1.25, 2.25],
      [-0.75, 1.25],
    ], dtype='float32')

    self.bbox = X_range

    np.random.seed(seed + 3)

    get_neg = lambda n: np.concatenate([
        np.random.normal(size=(4 * n, 2)) / 4 + center[None, :],
        np.random.uniform(size=(n, 2)) * (X_range[:, 1] - X_range[:, 0])[None, :] + X_range[None, :, 0],
      ], axis=0).astype('float32')

    data_neg = get_neg(4)
    data_neg_test = get_neg(64)

    data_test = np.concatenate([
      data_pos_test,
      data_neg_test
    ], axis=0)

    labels_test = np.concatenate([
      np.ones(data_pos_test.shape[0], dtype='float32'),
      np.zeros(data_neg_test.shape[0], dtype='float32')
    ])

    super(Moons, self).__init__(
      data_pos, data_neg,
      data_test, labels_test,
      batch_size=32,
      seed=seed
    )

  def bounding_box(self):
    return self.bbox

  def network_pool(self):
    from .common import get_dense_network_pool

    return get_dense_network_pool(
      num_features=2,
      n=16,
      code_size=1,
      latent_size=2
    )

moons = lambda seed: lambda: Moons(seed)