from .moons import moons

from .higgs import higgs
from .susy import susy

from .kdd import kdd

from .mnist import mnist
from .cifar import cifar10
from .omniglot import omniglot

__all__ = [
  'moons',
  'higgs',
  'susy',
  'mnist',
  'cifar10',
  'omniglot',
  'kdd'
]
