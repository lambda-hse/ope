import numpy as np

from .meta import *

__all__ = [
  'omniglot'
]

N = 16
LATENT_SIZE = 8 * N
CODE_SIZE = 8 * N

def _network_pool():
  from craynn import net, achain
  from craynn import select, concat

  from craynn import conv, conv_1x1
  from craynn import dense, flatten, batch_norm, batch_normed
  from craynn import deconv, upscale, reshape

  from craynn import max_pool, global_max_pool
  from craynn import default_nonlinearity, linear
  from craynn import ones_init, zeros_init, constant_parameter

  b0 = constant_parameter(0)

  forward = lambda conv=conv: achain(
    conv(2 * N, kernel_size=(5, 5), stride=2),
    conv(2 * N, kernel_size=(5, 5), stride=2),
    conv(3 * N), conv(3 * N), max_pool(),
    conv(4 * N), conv(4 * N), max_pool(),
    conv(6 * N),
  )

  backward = lambda deconv=deconv: achain(
    deconv(6 * N),
    upscale(), deconv(4 * N), deconv(4 * N),
    upscale(), deconv(3 * N), deconv(3 * N),
    deconv(2 * N, kernel_size=(5, 5), stride=2),
    deconv(2 * N, kernel_size=(5, 5), stride=2),
    deconv(1, kernel_size=(1, 1), activation=linear())
  )

  return {
    'classifier' : lambda: net((None, 105, 105, 1))(
      forward(),
      flatten(2),
      dense(1, activation=linear()),
      flatten(1)
    ),

    'biclassifier': lambda: net((None, 105, 105, 1), (None, LATENT_SIZE))(
      [
        (
          select[0], forward(), flatten(2)
        ),
        select[1]
      ],

      concat(),
      dense(1, activation=linear()),
      flatten(1)
    ),

    'generator' : lambda: net((None, LATENT_SIZE))(
      reshape((None, 1, 1, LATENT_SIZE)),
      backward(
        deconv=lambda *args, **kwargs: batch_normed(
          axes=(0, ), gamma=ones_init(), beta=zeros_init()
        )[deconv](*args, **kwargs)
      )
    ),

    'inference': lambda: net((None, 105, 105, 1))(
      forward(), flatten(2),
      dense(LATENT_SIZE, activation=linear()),
    ),

    'encoder' : lambda: net((None, 105, 105, 1))(
      forward(), flatten(2),
      dense(CODE_SIZE, activation=linear()),
    ),

    'decoder' : lambda: net((None, CODE_SIZE))(
      reshape((None, 1, 1, CODE_SIZE)),
      backward()
    ),

    'dense_classifier' : lambda : net((None, CODE_SIZE))(
      dense(4 * N), dense(2 * N),
      dense(1, activation=linear()),
      flatten(1)
    ),

    'svdd' : lambda : net((None, 105, 105, 1))(
      forward(conv=lambda *args, **kwargs: conv(*args, **kwargs, b=b0)),
      flatten(2),
      dense(1, b=b0, activation=linear()),
      flatten(1)
    ),

    'shallow_classifier': lambda: net((None, CODE_SIZE))(
      dense(2 * N),
      dense(1, activation=linear()), flatten(1)
    )
  }

class Omniglot(GroupedDataset):
  def __init__(self, normal, seed, root=None):
    from craynn import datasets, utils

    self.normal = normal

    data_train, alphabets_train, _, mapping = datasets.dataflow(
      datasets.download_omniglot('omniglot'),
      (lambda path: datasets.read_omniglot(path, return_mapping=True)) @ datasets.pickled('omniglot/omniglot.pickled')
    )(root)

    self.normal_indx = mapping[normal]

    data_neg_test, alphabets_test, _ = datasets.dataflow(
      datasets.download_omniglot_test('omniglot'),
      datasets.read_omniglot @ datasets.pickled('omniglot/omniglot-test.pickled')
    )(root)

    indx_normal, = np.where(alphabets_train == self.normal_indx)
    data_pos = data_train[indx_normal]

    data_pos, data_pos_test = utils.split(data_pos, split_ratios=(1, 1), seed=seed)

    indx_neg, = np.where(alphabets_train != self.normal_indx)
    data_neg = data_train[indx_neg]
    groups = alphabets_train[indx_neg]

    data_test = np.concatenate([
      data_pos_test,
      data_neg_test
    ], axis=0)

    labels_test = np.concatenate([
      np.ones(data_pos_test.shape[0], dtype='int32'),
      np.zeros(data_neg_test.shape[0], dtype='int32')
    ], axis=0)

    super(Omniglot, self).__init__(
      data_pos.astype('float32'), data_neg.astype('float32'),
      data_test.astype('float32'), labels_test,
      batch_size=8,
      neg_group_indx=groups,
      seed=seed
    )

  def network_pool(self):
    return _network_pool()

  def name(self):
    return 'Omniglot-%s' % (self.normal, )

omniglot = lambda normal, seed, root=None: lambda: Omniglot(
  normal, seed=seed, root=root
)