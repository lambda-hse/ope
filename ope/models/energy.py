from .meta import *
from .utils import default_chain

__all__ = [
  'energy_ope',
  'energy_oc'
]

class EnergyOPE(Model):
  def __init__(
    self, classifier, X_pos, X_neg, alpha=None, eps=0.9,
    persistent=False, chain=default_chain,
    bounding_box=None
  ):
    import tensorflow as tf
    from craynn.utils import tensor_shape

    self.classifier = classifier

    num_pseudo = tensor_shape(X_pos)[0]
    self.chain = chain(
      classifier,
      num_samples=num_pseudo,
      initial=None if persistent else X_pos,
      bounding_box=bounding_box
    )

    self.X_pseudo = self.chain()

    self.p_pos, = classifier(X_pos)
    self.p_pseudo, = classifier(self.X_pseudo)
    if alpha is not None:
      self.p_neg, = classifier(X_neg)

    self.loss_pos = tf.reduce_mean(tf.nn.softplus(-self.p_pos))
    self.loss_pseudo = tf.reduce_mean(self.p_pseudo)
    if alpha is not None:
      self.loss_neg = tf.reduce_mean(tf.nn.softplus(self.p_neg))

    c_pseudo = tf.constant(1 - eps, dtype='float32')

    reg = 1e-2 * tf.reduce_mean(self.p_pseudo ** 2)

    if alpha is not None:
      c_neg = tf.constant(alpha, dtype='float32')
      self.loss = self.loss_pos + c_neg * self.loss_neg + c_pseudo * self.loss_pseudo + reg
    else:
      self.loss = self.loss_pos + c_pseudo * self.loss_pseudo + reg

    self.gradients = list(zip(
      classifier.variables(),
      tf.gradients(
        self.loss,
        classifier.variables(),
        stop_gradients=[X_pos, self.X_pseudo] + ([X_neg] if alpha is not None else [])
      )
    ))
    
    super(EnergyOPE, self).__init__(alpha=alpha, eps=eps)

  def __call__(self, X):
    return self.classifier(X)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    return self.chain.reset()

  def updates(self):
    return self.chain.updates()

energy_ope = lambda eps, persistent=False, chain=default_chain: \
  lambda classifier: \
    lambda X_pos, X_neg, alpha, bounding_box: \
      EnergyOPE(
        classifier, X_pos, X_neg, alpha=alpha, eps=eps,
        persistent=persistent, chain=chain, bounding_box=bounding_box
      )

energy_oc = lambda eps, persistent=False, chain=default_chain: \
  lambda classifier: \
    lambda X_pos, X_neg, alpha, bounding_box: \
      EnergyOPE(
        classifier, X_pos, X_neg, alpha=None, eps=eps,
        persistent=persistent, chain=chain, bounding_box=bounding_box
      )