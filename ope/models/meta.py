__all__ = [
  'Model',
]

class Model(object):
  def __init__(self, **hyperparameters):
    self._hyperparameters = hyperparameters

  def hyperparameters(self):
    return self._hyperparameters

  def loss_and_grads(self):
    raise NotImplementedError

  def __call__(self, X):
    raise NotImplementedError

  def reset(self):
    raise NotImplementedError

  def updates(self):
    raise NotImplementedError

  def name(self):
    return self.__class__.__name__