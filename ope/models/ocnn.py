from .meta import Model

__all__ = [
  'nn_ae_one_class'
]

class OneClassNNAE(Model):
  def __init__(self, classifier, encoder, decoder, X_pos, X_neg, nu, alpha=1):
    import tensorflow as tf

    self.encoder = encoder
    self.decoder = decoder
    self.classifier = classifier

    self.X_code, = encoder(X_pos)
    self.X_rec, = decoder(self.X_code)

    self.predictions, = classifier(self.X_code)

    loss_rec = tf.reduce_mean((self.X_rec - X_pos) ** 2)

    zero = tf.constant(0, dtype='float32')

    self.R_raw = tf.Variable(
      initial_value=tf.zeros(shape=tuple(), dtype='float32')
    )
    self.R = tf.nn.softplus(self.R_raw)

    self.rnu = tf.constant(1 / nu, dtype='float32')

    reg, _ = classifier.reg_l2()

    self.loss = 0.5 * reg + self.rnu * tf.reduce_mean(
      tf.maximum(zero, self.R - self.predictions)
    ) - self.R

    ae_vars = encoder.variables() + decoder.variables()

    self.gradients = list(zip(
      ae_vars,
      tf.gradients(
        loss_rec, ae_vars,
        stop_gradients=[X_pos]
      )
    )) + list(zip(
      classifier.variables() + [self.R_raw],
      tf.gradients(
        self.loss,
        classifier.variables() + [self.R_raw],
        stop_gradients=[self.X_code]
      )
    ))
    
    super(OneClassNNAE, self).__init__(alpha=alpha)

  def __call__(self, X):
    code, = self.encoder(X)
    return self.classifier(code)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    return [self.R_raw.initializer]

  def updates(self):
    import tensorflow as tf

    return tf.no_op()

nn_ae_one_class = lambda nu=0.1: lambda shallow_classifier, encoder, decoder: lambda X_pos, X_neg, alpha, bounding_box: \
  OneClassNNAE(shallow_classifier, encoder, decoder, X_pos, X_neg, nu=nu, alpha=alpha)