from .meta import Model

__all__ = [
  'deep_energy_ope',
  'cycle_deep_energy_ope',
  'deep_energy_oc',

  'bideep_energy_ope',
  'bideep_energy_oc'
]

class DeepEnergy(Model):
  def __init__(self, classifier, generator, X_pos, X_neg, alpha=1, eps=0.9):
    import tensorflow as tf
    from craynn.layers import get_output_shape
    from craynn.utils import tensor_shape

    self.classifier = classifier
    self.generator = generator

    assert len(generator.inputs) == 1
    latent_shape = get_output_shape(generator.inputs[0])

    num_pseudo = tensor_shape(X_pos)[0]

    self.generator_latent = tf.random_normal(
      shape=(num_pseudo, ) + latent_shape[1:],
      dtype='float32'
    )

    self.X_pseudo, = generator(self.generator_latent)

    bn_scale = generator.variables(normalization_scales=True)
    assert len(bn_scale) > 0

    pseudo_entropy = sum([
      tf.reduce_sum(tf.log(gamma ** 2))
      for gamma in bn_scale
    ])

    self.p_pseudo, = classifier(self.X_pseudo)

    self.loss_generator = -tf.reduce_mean(self.p_pseudo) - 0.5 * pseudo_entropy

    self.p_pos, = classifier(X_pos)
    self.p_neg, = classifier(X_neg)

    self.loss_pos = tf.reduce_mean(tf.nn.softplus(-self.p_pos))
    self.loss_neg = tf.reduce_mean(tf.nn.softplus(self.p_neg))
    self.loss_pseudo = tf.reduce_mean(self.p_pseudo)

    c_neg = tf.constant(alpha, dtype='float32')
    c_pseudo = tf.constant(1 - eps, dtype='float32')

    self.loss = self.loss_pos + c_neg * self.loss_neg + c_pseudo * self.loss_pseudo

    self.gradients = list(zip(
      generator.variables(),
      tf.gradients(self.loss_generator, generator.variables())
    )) + list(zip(
      classifier.variables(),
      tf.gradients(self.loss, classifier.variables(), stop_gradients=[X_pos, X_neg, self.X_pseudo])
    ))
    
    super(DeepEnergy, self).__init__(alpha=alpha, eps=eps)

  def __call__(self, X):
    return self.classifier(X)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    import tensorflow as tf
    return tf.no_op()

  def updates(self):
    import tensorflow as tf
    return tf.no_op()

deep_energy_ope = lambda eps=0.9: lambda classifier, generator: lambda X_pos, X_neg, alpha, bounding_box: \
  DeepEnergy(classifier, generator, X_pos, X_neg, alpha=alpha, eps=eps)

class CycleDeepEnergy(Model):
  def __init__(self, classifier, generator, inference, X_pos, X_neg, alpha=1, eps=0.9, cycle_coef=1e-1):
    import tensorflow as tf
    from craynn.layers import get_output_shape
    from craynn.utils import tensor_shape

    self.classifier = classifier
    self.generator = generator
    self.inference = inference

    assert len(generator.inputs) == 1
    latent_shape = get_output_shape(generator.inputs[0])
    num_pseudo = tensor_shape(X_pos)[0]

    self.generator_latent = tf.random_normal(
      shape=(num_pseudo,) + latent_shape[1:],
      dtype='float32'
    )

    self.X_pseudo, = generator(self.generator_latent)

    self.latent_inferred, = inference(self.X_pseudo)
    self.loss_inference = tf.reduce_mean((self.generator_latent - self.latent_inferred) ** 2)

    self.latent_pos, = inference(X_pos)
    self.X_pseudo_pos, = generator(self.latent_pos)
    self.cycle_loss = tf.reduce_mean(tf.log((self.X_pseudo_pos - X_pos) ** 2 + 1))

    bn_scale = generator.variables(normalization_scales=True)

    pseudo_entropy = sum([
      tf.reduce_sum(tf.log(gamma ** 2))
      for gamma in bn_scale
    ])

    self.p_pseudo, = classifier(self.X_pseudo)

    self.cycle_coef = tf.constant(cycle_coef, dtype='float32')

    self.loss_generator = -tf.reduce_mean(self.p_pseudo) - 0.5 * pseudo_entropy + self.cycle_coef * self.cycle_loss

    self.p_pos, = classifier(X_pos)
    self.p_neg, = classifier(X_neg)

    self.loss_pos = tf.reduce_mean(tf.nn.softplus(-self.p_pos))
    self.loss_neg = tf.reduce_mean(tf.nn.softplus(self.p_neg))
    self.loss_pseudo = tf.reduce_mean(self.p_pseudo)

    c_neg = tf.constant(alpha, dtype='float32')
    c_pseudo = tf.constant(1 - eps, dtype='float32')

    self.loss = self.loss_pos + c_neg * self.loss_neg + c_pseudo * self.loss_pseudo

    self.gradients = list(zip(
      inference.variables(),
      tf.gradients(
        self.loss_inference,
        inference.variables()
      )
    )) + list(zip(
      generator.variables(),
      tf.gradients(
        self.loss_generator,
        generator.variables(),
      )
    )) + list(zip(
      classifier.variables(),
      tf.gradients(
        self.loss,
        classifier.variables(),
        stop_gradients=[X_pos, X_neg, self.X_pseudo]
      )
    ))
    
    super(CycleDeepEnergy, self).__init__(eps=eps, alpha=alpha, cycle_coef=cycle_coef)

  def __call__(self, X):
    return self.classifier(X)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    import tensorflow as tf
    return tf.no_op()

  def updates(self):
    import tensorflow as tf
    return tf.no_op()

cycle_deep_energy_ope = lambda eps=0.9, cycle_coef=0.1: \
  lambda classifier, generator, inference: lambda X_pos, X_neg, alpha, bounding_box: \
    CycleDeepEnergy(
      classifier, generator, inference, X_pos, X_neg,
      alpha=alpha, eps=eps, cycle_coef=cycle_coef
    )


class BiDeepEnergy(Model):
  def __init__(self, biclassifier, generator, inference, X_pos, X_neg, alpha=None, eps=0.9):
    import numpy as np
    import tensorflow as tf
    from craynn import glorot_scaling
    from craynn.layers import get_output_shape
    from craynn.utils import tensor_shape

    self.classifier = biclassifier
    self.generator = generator
    self.inference = inference

    assert len(generator.inputs) == 1
    latent_shape = get_output_shape(generator.inputs[0])
    num_pseudo = tensor_shape(X_pos)[0]

    self.true_latent = tf.random_normal(
      shape=(num_pseudo,) + latent_shape[1:],
      dtype='float32'
    )

    self.X_pseudo, = generator(self.true_latent)

    self.inferred_pos, = inference(X_pos)
    if alpha is not None:
      self.inferred_neg, = inference(X_neg)
    else:
      self.inferred_neg = None

    self.energy_gen, = biclassifier(self.X_pseudo, self.true_latent)
    self.energy_pos, = biclassifier(X_pos, self.inferred_pos)
    if alpha is not None:
      self.energy_neg, = biclassifier(X_neg, self.inferred_neg)
    else:
      self.energy_neg = None

    pseudo_entropy = 0.5 * sum([
      tf.reduce_sum(tf.log(gamma ** 2))
      for gamma in generator.variables(normalization_scales=True)
    ])

    self.loss_generator = -tf.reduce_mean(self.energy_gen) - 0.5 * pseudo_entropy

    self.loss_pos = tf.reduce_mean(tf.nn.softplus(-self.energy_pos))
    self.loss_pseudo = tf.reduce_mean(self.energy_gen)
    if alpha is not None:
      self.loss_neg = tf.reduce_mean(tf.nn.softplus(self.energy_neg))
    else:
      self.loss_neg = None

    c_pseudo = tf.constant(1 - eps, dtype='float32')

    reg = 1e-2  * tf.reduce_mean(self.energy_gen ** 2)

    if alpha is not None:
      c_neg = tf.constant(alpha, dtype='float32')

      self.loss = self.loss_pos + c_neg * self.loss_neg + c_pseudo * self.loss_pseudo + reg
    else:
      self.loss = self.loss_pos + c_pseudo * self.loss_pseudo + reg

    self.gradients = list(zip(
      generator.variables() + inference.variables(),
      tf.gradients(
        self.loss_generator,
        generator.variables() + inference.variables(),
      )
    )) + list(zip(
      biclassifier.variables(),
      tf.gradients(
        self.loss,
        biclassifier.variables(),
      )
    ))

    super(BiDeepEnergy, self).__init__(eps=eps, alpha=alpha)

  def __call__(self, X):
    inferred, = self.inference(X)
    return self.classifier(X, inferred)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    import tensorflow as tf
    return tf.no_op()

  def updates(self):
    import tensorflow as tf
    return tf.no_op()


bideep_energy_ope = lambda eps=0.9: \
  lambda biclassifier, generator, inference: lambda X_pos, X_neg, alpha, bounding_box: \
    BiDeepEnergy(
      biclassifier, generator, inference, X_pos, X_neg,
      alpha=alpha, eps=eps
    )

bideep_energy_oc = lambda eps=0.9: \
  lambda biclassifier, generator, inference: lambda X_pos, X_neg, alpha, bounding_box: \
    BiDeepEnergy(
      biclassifier, generator, inference, X_pos, X_neg,
      alpha=None, eps=eps
    )

class DeepEnergyOneClass(Model):
  def __init__(self, classifier, generator, X_pos, eps=0.9):
    import tensorflow as tf
    from craynn.layers import get_output_shape
    from craynn.utils import tensor_shape

    self.classifier = classifier
    self.generator = generator

    assert len(generator.inputs) == 1
    latent_shape = get_output_shape(generator.inputs[0])
    num_pseudo = tensor_shape(X_pos)[0]

    self.generator_latent = tf.random_normal(
      shape=(num_pseudo,) + latent_shape[1:],
      dtype='float32'
    )

    self.X_pseudo, = generator(self.generator_latent)

    bn_scale = generator.variables(normalization_scales=True)

    pseudo_entropy = sum([
      tf.reduce_sum(tf.log(gamma ** 2))
      for gamma in bn_scale
    ])

    self.p_pseudo, = classifier(self.X_pseudo)

    self.loss_generator = -tf.reduce_mean(self.p_pseudo) - pseudo_entropy

    self.p_pos, = classifier(X_pos)

    self.loss_pos = tf.reduce_mean(tf.nn.softplus(-self.p_pos))
    self.loss_pseudo = tf.reduce_mean(self.p_pseudo)

    c_pseudo = tf.constant(1 - eps, dtype='float32')

    self.loss = self.loss_pos + c_pseudo * self.loss_pseudo

    self.gradients = list(zip(
      generator.variables(),
      tf.gradients(self.loss_generator, generator.variables())
    )) + list(zip(
      classifier.variables(),
      tf.gradients(self.loss, classifier.variables(), stop_gradients=[X_pos, self.X_pseudo])
    ))

    super(DeepEnergyOneClass, self).__init__(eps=eps)

  def __call__(self, X):
    return self.classifier(X)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    import tensorflow as tf
    return tf.no_op()

  def updates(self):
    import tensorflow as tf
    return tf.no_op()

deep_energy_oc = lambda eps=1e-1: lambda classifier, generator: lambda X_pos, X_neg, alpha, bounding_box: \
  DeepEnergyOneClass(classifier, generator, X_pos, eps=eps)