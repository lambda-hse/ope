from .utils import clip

__all__ = [
  'random_walk_chain',
]

class RandomWalkChain(object):
  def __init__(self, f, num_samples, step_size=1e-2, initial=None, bounding_box=None):
    import tensorflow as tf
    from craynn.layers import get_output_shape

    shape = get_output_shape(f.inputs[0])[1:]

    self.step_size = tf.constant(step_size, dtype='float32')

    if initial is None:
      self.X_initial = tf.Variable(
        initial_value=tf.random_normal(shape=(num_samples, ) + shape),
        dtype='float32'
      )
    else:
      self.X_initial = initial

    r = tf.random_normal(shape=(num_samples, ) + shape, dtype='float32')

    self.X_pseudo = clip(self.X_initial + r * step_size, shape=shape, bounding_box=bounding_box)


    if initial is None:
      self._reset = [self.X_initial.initializer]
    else:
      self._reset = tf.no_op()

    if initial is None:
      self.mcmc_update = tf.assign(self.X_initial, self.X_pseudo)
    else:
      self.mcmc_update = tf.no_op()

  def __call__(self):
    return self.X_pseudo

  def reset(self):
    return self._reset

  def updates(self):
    return self.mcmc_update

random_walk_chain = lambda step_size=1e-2: \
  lambda f, num_samples, bounding_box, initial=None: RandomWalkChain(
    f, num_samples=num_samples, step_size=step_size,
    initial=initial, bounding_box=bounding_box
  )