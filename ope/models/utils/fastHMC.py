from .utils import clip

__all__ = [
  'fast_hmc_chain',
]

class FastHMCChain(object):
  def __init__(self, f, num_samples, num_steps=8, step_size=1e-2, initial=None, bounding_box=None):
    import tensorflow as tf
    from craynn.layers import get_output_shape

    shape = get_output_shape(f.inputs[0])[1:]

    log_prob = lambda x: f(x)[0]

    self.step_size = tf.constant(step_size, dtype='float32')

    if initial is None:
      self.X_initial = tf.Variable(
        initial_value=tf.random_normal(shape=(num_samples, ) + shape),
        dtype='float32'
      )
    else:
      self.X_initial = initial

    def step(position, v):
      U = log_prob(position)
      grad, = tf.gradients(U, position)

      velocity = v + self.step_size * grad
      new_position = clip(position + self.step_size * velocity, shape=shape, bounding_box=bounding_box)

      return new_position, velocity

    U = log_prob(self.X_initial)
    grad, = tf.gradients(U, self.X_initial)
    grad_norm = tf.sqrt(
      tf.reduce_sum(grad ** 2, axis=list(range(1, len(shape) + 1)))
    )

    broadcast = (slice(None, None, None), ) + tuple([ None for _ in shape ])
    v0 = tf.random_normal(shape=(num_samples, ) + shape, dtype='float32') * grad_norm[broadcast]

    self.X_pseudo, _ = tf.while_loop(
      cond=lambda *args: True,
      loop_vars=(self.X_initial, v0),
      body=step,
      parallel_iterations=num_samples // 8,
      back_prop=False,
      maximum_iterations=num_steps
    )


    if initial is None:
      self._reset = [self.X_initial.initializer]
    else:
      self._reset = tf.no_op()

    if initial is None:
      self.mcmc_update = tf.assign(self.X_initial, self.X_pseudo)
    else:
      self.mcmc_update = tf.no_op()

  def __call__(self):
    return self.X_pseudo

  def reset(self):
    return self._reset

  def updates(self):
    return self.mcmc_update

fast_hmc_chain = lambda num_steps=4, step_size=1e-2: \
  lambda f, num_samples, bounding_box, initial=None: FastHMCChain(
    f, num_samples=num_samples, num_steps=num_steps, step_size=step_size,
    initial=initial, bounding_box=bounding_box
  )