from .utils import clip

__all__ = [
  'sgd_chain',
  'rmsprop_chain'
]

class SGDChain(object):
  def __init__(self, f, num_samples, num_steps=4, step_size=1e-2, noise=None, initial=None, bounding_box=None):
    import tensorflow as tf
    from craynn.layers import get_output_shape

    shape = get_output_shape(f.inputs[0])[1:]

    log_prob = lambda x: f(x)[0]

    self.step_size = tf.constant(step_size, dtype='float32')

    if initial is None:
      self.X_initial = tf.Variable(
        initial_value=tf.random_normal(shape=(num_samples, ) + shape),
        dtype='float32'
      )
    else:
      self.X_initial = initial

    if noise is not None:
      noise_scale = tf.constant(noise, dtype='float32')

      def step(position, r, i):
        U = log_prob(position)
        grad, = tf.gradients(U, position)
        grad_norm = tf.sqrt(
          tf.reduce_sum(grad ** 2, axis=list(range(1, len(shape) + 1)))
        )

        broadcast = (slice(None, None, None), ) + tuple([None for _ in shape])
        step = grad + r[i] * noise_scale * grad_norm[broadcast]

        return clip(position + self.step_size * step, shape=shape, bounding_box=bounding_box), r, i + 1

      r0 = tf.random.normal(shape=(num_steps, num_samples,) + shape, dtype='float32')
      i0 = tf.constant(0, dtype='int32')

      self.X_pseudo, _, _ = tf.while_loop(
        cond=lambda x, r, i: tf.less(i, num_steps),
        loop_vars=(self.X_initial, r0, i0),
        body=step,
        parallel_iterations=num_samples // 8,
        back_prop=False,
      )
    else:
      def step(position):
        U = log_prob(position)
        grad, = tf.gradients(U, position)
        return clip(position + self.step_size * grad, shape=shape, bounding_box=bounding_box)

      self.X_pseudo = tf.while_loop(
        cond=lambda *args: True,
        loop_vars=(self.X_initial, ),
        body=step,
        parallel_iterations=num_samples // 8,
        back_prop=False,
        maximum_iterations=num_steps
      )

    if initial is None:
      self._reset = [self.X_initial.initializer]
    else:
      self._reset = tf.no_op()

    if initial is None:
      self.mcmc_update = tf.assign(self.X_initial, self.X_pseudo)
    else:
      self.mcmc_update = tf.no_op()

  def __call__(self):
    return self.X_pseudo

  def reset(self):
    return self._reset

  def updates(self):
    return self.mcmc_update

sgd_chain = lambda num_steps=4, step_size=1e-2, noise=None: \
  lambda f, num_samples, bounding_box, initial=None: SGDChain(
    f, num_samples=num_samples, num_steps=num_steps, step_size=step_size,
    noise=noise, initial=initial, bounding_box=bounding_box
  )

class RMSPropChain(object):
  def __init__(self, f, num_samples, num_steps=4, step_size=1e-2, rho=0.9, noise=None, initial=None, bounding_box=None):
    import tensorflow as tf
    from craynn.layers import get_output_shape

    shape = get_output_shape(f.inputs[0])[1:]

    log_prob = lambda x: f(x)[0]

    self.step_size = tf.constant(step_size, dtype='float32')

    if initial is None:
      self.X_initial = tf.Variable(
        initial_value=tf.random_normal(shape=(num_samples, ) + shape, dtype='float32'),
        dtype='float32'
      )
    else:
      self.X_initial = initial

    self.second_momentum = tf.Variable(
      initial_value=tf.zeros(shape=(num_samples, ) + shape, dtype='float32')
    )

    crho = tf.constant(1 - rho, dtype='float32')
    rho = tf.constant(rho, dtype='float32')
    eps = tf.constant(1e-4, dtype='float32')

    if noise is not None:
      def step(x, second_momentum, r, i):
        U = log_prob(x)
        grad, = tf.gradients(U, x)

        new_second_momentum = rho * second_momentum + crho * grad ** 2
        step = grad / tf.sqrt(new_second_momentum + eps) + r[i]

        new_x = clip(x + self.step_size * step, shape=shape, bounding_box=bounding_box)

        return new_x, new_second_momentum, r, i + 1

      r0 = tf.random.normal(shape=(num_steps, num_samples,) + shape, dtype='float32')
      i0 = tf.constant(0, dtype='int32')

      self.X_pseudo, updated_second_momentum, _, _ = tf.while_loop(
        cond=lambda x, m, r, i: tf.less(i, num_steps),
        loop_vars=(self.X_initial, self.second_momentum, r0, i0),
        body=step,
        parallel_iterations=num_samples // 8,
        back_prop=False,
      )
    else:
      def step(x, second_momentum):
        U = log_prob(x)
        grad, = tf.gradients(U, x)

        new_second_momentum = rho * second_momentum + crho * grad ** 2
        step = grad / tf.sqrt(new_second_momentum + eps)

        new_x = clip(x + self.step_size * step, shape=shape, bounding_box=bounding_box)

        return new_x, new_second_momentum

      self.X_pseudo, updated_second_momentum = tf.while_loop(
        cond=lambda *args: True,
        loop_vars=(self.X_initial, self.second_momentum),
        body=step,
        parallel_iterations=num_samples // 8,
        back_prop=False,
        maximum_iterations=num_steps
      )

    self.momentum_upd = tf.assign(self.second_momentum, updated_second_momentum)

    if initial is None:
      self._reset = [
        self.X_initial.initializer,
        self.second_momentum.initializer
      ]
    else:
      self._reset = [
        self.second_momentum.initializer
      ]

    if initial is None:
      self._updates = tf.group([
        tf.assign(self.X_initial, self.X_pseudo),
        self.momentum_upd
      ])
    else:
      self._updates = self.momentum_upd

  def __call__(self):
    return self.X_pseudo

  def reset(self):
    return self._reset

  def updates(self):
    return self._updates

rmsprop_chain = lambda num_steps=4, step_size=1e-2, rho=0.9, noise=None: \
  lambda f, num_samples, bounding_box, initial=None: RMSPropChain(
    f, num_samples=num_samples, num_steps=num_steps, step_size=step_size, rho=rho,
    noise=noise, initial=initial, bounding_box=bounding_box
  )