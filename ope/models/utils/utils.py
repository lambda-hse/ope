__all__ = [
  'combine',
  'logit_binary_crossentropy',
  'clip'
]

def combine(X_pos, X_neg, alpha, X_pseudo=None, eps=None):
  import tensorflow as tf

  n_pos = tf.shape(X_pos)[0]
  n_neg = tf.shape(X_neg)[0]

  if X_pseudo is not None:
    X = tf.concat([
      X_pos, X_neg, X_pseudo
    ], axis=0)

    c_neg = tf.constant(alpha, dtype='float32')
    c_pseudo = tf.constant((1 - eps), dtype='float32')

    n_pseudo = tf.shape(X_pseudo)[0]
    w = tf.concat([
      tf.ones(n_pos, dtype='float32') / tf.cast(n_pos, dtype='float32'),
      c_neg * tf.ones(n_neg, dtype='float32') / tf.cast(n_neg, dtype='float32'),
      c_pseudo * tf.ones(n_pseudo, dtype='float32') / tf.cast(n_pseudo, dtype='float32')
    ], axis=0)

    y = tf.concat([
      tf.ones(n_pos, dtype='float32'),
      tf.zeros(n_neg, dtype='float32'),
      tf.zeros(n_pseudo, dtype='float32')
    ], axis=0)

    return X, y, w
  else:
    X = tf.concat([
      X_pos, X_neg
    ], axis=0)

    c_neg = tf.constant(alpha, dtype='float32')
    w = tf.concat([
      tf.ones(n_pos, dtype='float32') / tf.cast(n_pos, dtype='float32'),
      c_neg * tf.ones(n_neg, dtype='float32') / tf.cast(n_neg, dtype='float32')
    ], axis=0)

    y = tf.concat([
      tf.ones(n_pos, dtype='float32'),
      tf.zeros(n_neg, dtype='float32'),
    ], axis=0)

    return X, y, w

def logit_binary_crossentropy(f, X, y, weights=None):
  import tensorflow as tf

  predictions, = f(X)
  losses = \
    y * tf.nn.softplus(-predictions) + (1 - y) * tf.nn.softplus(predictions)

  if weights is not None:
    loss = tf.reduce_sum(weights * losses)
  else:
    loss = tf.reduce_sum(losses)

  gradients = list(zip(
    f.variables(),
    tf.gradients(
      loss,
      f.variables(),
      stop_gradients=[X]
    )
  ))

  return predictions, loss, gradients

def clip(X, shape, bounding_box=None):
  import tensorflow as tf

  if bounding_box is not None:
    broadcast = tuple([slice(None, None, None) for _ in shape])

    X_min = tf.constant(bounding_box[broadcast + (0, )], dtype='float32')
    X_max = tf.constant(bounding_box[broadcast + (1, )], dtype='float32')

    return tf.clip_by_value(X, X_min[broadcast], X_max[broadcast])
  else:
    return X