__all__ = [
  'hmc_chain'
]

class HMCChain(object):
  def __init__(self, f, num_samples, num_steps=4, step_size=1e-2, initial=None, bounding_box=None):
    import tensorflow as tf
    import tensorflow_probability as tfp

    from craynn.layers import get_output_shape
    from craynn import utils

    from .utils import clip

    shape = get_output_shape(f.inputs[0])[1:]

    broadcast = tuple([ slice(None, None, None) for _ in shape ])

    log_prob = lambda x: f(x)[0]

    self.step_size = tf.Variable(initial_value=step_size, dtype='float32', trainable=False)

    self.kernel = tfp.mcmc.HamiltonianMonteCarlo(
      log_prob, self.step_size, num_leapfrog_steps=4,
      step_size_update_fn=tfp.mcmc.make_simple_step_size_update_policy(None),
    )

    if initial is None:
      self.hmc_state = tf.Variable(
        initial_value=tf.random_normal(shape=(num_samples,) + shape, dtype='float32', ),
        validate_shape=True,
        expected_shape=(num_samples, ) + shape
      )
      self.X_pseudo_raw, _ = tfp.mcmc.sample_chain(
        num_results=2,
        current_state=self.hmc_state,
        kernel=self.kernel,
        num_burnin_steps=num_steps,
        parallel_iterations=max([num_samples // 4, 1])
      )

      self.X_pseudo = clip(self.X_pseudo_raw[-1], shape=shape, bounding_box=bounding_box)
      self.mcmc_update = tf.assign(self.hmc_state, self.X_pseudo)
    else:
      self.hmc_state = None

      self.X_pseudo_raw, _ = tfp.mcmc.sample_chain(
        num_results=2,
        current_state=initial,
        kernel=self.kernel,
        num_burnin_steps=num_steps
      )

      self.X_pseudo = clip(self.X_pseudo_raw[-1], shape=shape, bounding_box=bounding_box)
      self.mcmc_update = tf.no_op()

    if self.hmc_state is not None:
      self._reset = utils.reset([
        self.hmc_state,
        self.step_size
      ])
    else:
      self._reset = utils.reset([
        self.step_size
      ])

  def __call__(self):
    return self.X_pseudo

  def reset(self):
    return self._reset

  def updates(self):
    return self.mcmc_update

hmc_chain = lambda num_steps=4, step_size=1e-2: \
  lambda f, num_samples, bounding_box, initial=None: HMCChain(
    f, num_samples=num_samples, num_steps=num_steps, step_size=step_size,
    initial=initial, bounding_box=bounding_box
  )