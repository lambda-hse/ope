__all__ = [
  'UniformGenerator',
  'uniform_generator',
  'uniform_chain'
]

class UniformGenerator(object):
  def __init__(self, num_samples, bounding_box):
    import tensorflow as tf

    broadcast = tuple([slice(None, None, None) for _ in range(len(bounding_box.shape) - 1)])

    z = tf.random_uniform(shape=(num_samples,) + bounding_box.shape[:-1], dtype='float32')
    X_min = tf.constant(bounding_box[broadcast + (0,)], dtype='float32')
    delta = tf.constant(bounding_box[broadcast + (1,)] - bounding_box[broadcast + (0,)], dtype='float32')

    self.X_uniform = z * delta[(None,) + broadcast] + X_min[(None,) + broadcast]

  def __call__(self):
    return self.X_uniform

  def reset(self):
    import tensorflow as tf
    return tf.no_op()

  def updates(self):
    import tensorflow as tf
    return tf.no_op()

uniform_generator = lambda num_samples, bounding_box: UniformGenerator(num_samples, bounding_box)


uniform_chain = lambda: \
  lambda f, num_samples, bounding_box, initial=None: UniformGenerator(
    num_samples=num_samples, bounding_box=bounding_box
  )