from .meta import Model
from .utils import *

__all__ = [
  'rae_oc'
]

class RobustAE(Model):
  def __init__(self, encoder, decoder, X_pos, creg=1e-1):
    import tensorflow as tf
    from craynn.utils import tensor_shape

    self.shape = tensor_shape(X_pos)

    self.encoder = encoder
    self.decoder = decoder

    self.code, = encoder(X_pos)
    self.reconstructed, = decoder(self.code)

    l = tf.constant(creg, dtype='float32')

    res = X_pos - self.reconstructed
    cres = tf.clip_by_value(res, -l, l)

    N = res - cres

    self.loss = tf.reduce_mean(
      (N - (X_pos - self.reconstructed)) ** 2
    )

    ae_vars = encoder.variables() + decoder.variables()

    self.gradients = list(zip(
      ae_vars,
      tf.gradients(
        self.loss,
        ae_vars,
        stop_gradients=[X_pos, N]
      )
    ))

    super(RobustAE, self).__init__()

  def loss_and_grads(self):
    return self.loss, self.gradients

  def __call__(self, X):
    import tensorflow as tf

    code, = self.encoder(X)
    rec, = self.decoder(code)
    return -tf.log(
      tf.reduce_mean(
        (X - rec) ** 2,
        axis=list(range(1, len(self.shape)))
      ) + 1e-3
    )

  def updates(self):
    import tensorflow as tf

    return tf.no_op()

  def reset(self):
    import tensorflow as tf

    return tf.no_op()


rae_oc = lambda reg=1: lambda encoder, decoder: lambda X_pos, X_neg, alpha, bounding_box: \
  RobustAE(encoder, decoder, X_pos, creg=reg)