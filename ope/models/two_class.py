from .meta import Model
from .utils import *

__all__ = [
  'cross_entropy'
]

class CrossEntropy(Model):
  def __init__(self, classifier, X_pos, X_neg, alpha=1):
    self.classifier = classifier

    self.X, self.y, self.w = combine(X_pos, X_neg, alpha=alpha)
    self.predictions, self.loss, self.gradients = logit_binary_crossentropy(
      classifier, self.X, self.y, weights=self.w
    )

    super(CrossEntropy, self).__init__(alpha=alpha)

  def __call__(self, X):
    return self.classifier(X)[0]

  def loss_and_grads(self):
    return self.loss, self.gradients

  def reset(self):
    import tensorflow as tf
    return tf.no_op()

  def updates(self):
    import tensorflow as tf
    return tf.no_op()

cross_entropy = lambda : lambda classifier: lambda X_pos, X_neg, alpha, bounding_box: CrossEntropy(
  classifier, X_pos, X_neg, alpha=alpha
)